(function ($) {
  'use strict';

  $(document).ready(function () {

    if (screen.width < 992) {
        $(".clean_rooms .fon").click(function(){
            $(".toggle-ul").hide();
            $(".icon-chevron-up").hide();
            $(".icon-chevron-down").show();
            $(".icon-chevron-down", this).toggle();
            $(".icon-chevron-up", this).toggle();
            $(".toggle-ul", this).toggle();
        });

        $("#close-mob-services").click(function(){
            $(".mob-services").hide();
        });

        $("#btn-services").click(function(){
            $(".mob-services").show();
        });

        $("#mob-search-li").click(function() {
            $(".search-form").toggle();
        });
    }

    $("#search-li").click(function() {
        $(".search-form").toggle();
    });

    $("#menu-btn").click(function(){
        $(".mobile-nav").toggle();
      $("#menu-icon").toggleClass('icon-navicon icon-close');
    });

      $(".consult-btn").click(function(){
          $(".consult-form").toggle();
      });

      //Order modal
      $(function(){
          $('.order-btn').click(function(){
              $('.order-form').slideDown();
              $('.modal-bg').css({"z-index":"100", "opacity":"1"})
          });
      });
      $(function() {
          $('#close-order').click(function () {
              $('.order-form').slideUp();
              $('.modal-bg').css({"z-index":"-1", "opacity":"0"})
          });
      });



      //Phone mask
      jQuery.fn.exists = function() {
          return jQuery(this).length;
      }

      if($('.phone').exists()){

          $('.phone').each(function(){
              $(this).mask("+38(999) 999-99-99");
          });
          $('.phone')
              .addClass('rfield')
              .removeAttr('required')
              .removeAttr('pattern')
              .removeAttr('title')
              .attr({'placeholder':'+38(___) ___-__-__'});
      }

  }); //end ready

}(jQuery));