(function ($) {
    'use strict';

    $(document).ready(function () {

        $("#slider-clients").owlCarousel({
          loop:true,
          margin:0,
          nav: false,
          dots: false,
          autoplay: true,
          autoplayTimeout: 6000,
          responsive:{
            0:{
              items:3
            },
            600:{
              items:4
            },
            1000:{
              items:6
            }
          }
        });

        $("#main-slider").owlCarousel({
          loop: true,
          margin: 0,
          dots: true,
          nav:true,
          autoplay: true,
          autoplayTimeout: 5000,
          responsive:{
            0:{
              items:1
            },
            600:{
              items:1
            },
            1000:{
              items:1
            }
          }
        });

        $( ".owl-prev" ).append( "<div class=\"icon-angle-left\"></div>" );
        $( ".owl-next" ).append( "<div class=\"icon-angle-right\"></div>" );

        document.getElementById('links').onclick = function (event) {
          event = event || window.event;
          var target = event.target || event.srcElement,
              link = target.src ? target.parentNode : target,
              options = {index: link, event: event},
              links = this.getElementsByTagName('a');
          blueimp.Gallery(links, options);
        };

        if (screen.width < 992) {
            $('.serv-item:last-child').click(function() {
                $('.serv-item:last-child').hide();
                $('.serv-item:nth-child(9)').show();
                $('.serv-item:nth-child(10)').show();
            });
        }

    }); //end ready

}(jQuery));