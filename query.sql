SELECT  t1.meta_value AS code,
        t2.post_title AS title,
        t5.name AS vendor,
		    m2.meta_value  AS testing,
        m1.meta_value  AS cost,
        m4.meta_value  AS longs

FROM wp_postmeta AS t1
LEFT JOIN wp_posts AS t2 ON t2.id = t1.post_id
LEFT JOIN wp_term_relationships AS t3 ON t3.object_id = t1.post_id

LEFT JOIN wp_term_taxonomy AS t4 ON t4.term_taxonomy_id = t3.term_taxonomy_id
LEFT JOIN wp_terms AS t5 ON t5.term_id = t4.parent
        LEFT  JOIN wp_postmeta AS m1  ON m1.post_id  = t2.ID AND m1.meta_key  = 'cost'
        LEFT  JOIN wp_postmeta AS m2  ON m2.post_id  = t2.ID AND m2.meta_key  = 'testing'
        LEFT  JOIN wp_postmeta AS m3  ON m3.post_id  = t2.ID AND m3.meta_key  = 'code'
        LEFT  JOIN wp_postmeta AS m4  ON m4.post_id  = t2.ID AND m4.meta_key  = 'long'
WHERE t1.meta_key = 'code' AND  t4.parent > 23 AND t4.parent IS NOT NULL